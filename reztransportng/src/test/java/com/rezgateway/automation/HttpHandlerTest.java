package com.rezgateway.automation;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.HttpResponse;

/**
 * Unit test for App;
 */
public class HttpHandlerTest {
	
	private JavaHttpHandler Handler = null;
	@Before
	public void setUp() {
      Handler = new JavaHttpHandler();
	}

	@Test
	public void checkGet() throws IOException {
       HttpResponse response = Handler.sendGET("http://www.google.lk");
       Assert.assertTrue("Response Code Validation", response.getRESPONSE_CODE() == 200);
	}

	@Test
	public void checkPost() throws IOException {
		 HttpResponse response = Handler.sendPOST("http://ptsv2.com/t/hjz00-1517998634/post","");
	     Assert.assertTrue("Response Message Validation", response.getRESPONSE().contains("Thank"));
	}

	@Test
	public void checkGetWithCon() {

	}

	@Test
	public void checkPostWithCon() {

	}

	@After
	public void tearDown() {

	}
}
