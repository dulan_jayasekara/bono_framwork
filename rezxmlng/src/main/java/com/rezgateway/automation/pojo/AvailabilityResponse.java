package com.rezgateway.automation.pojo;

import java.util.HashMap;
import java.util.Map;

public class AvailabilityResponse {
	
	private int 							HotelCount 			= 0;
	private String 							AvailabilityStatus 	= "N/A";
	private Map<String, Hotel> 				HotelList			= new HashMap<String, Hotel>();
	private String 							ErrorCode 			= null;
	private String 							ErrorDescription 	= null;
	
	/**
	 * @return the hotelCount
	 */
	public int getHotelCount() {
		return HotelCount;
	}
	/**
	 * @return the hotelList
	 */
	public Map<String, Hotel> getHotelList() {
		return HotelList;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return ErrorCode;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return ErrorDescription;
	}
	/**
	 * @param hotelCount the hotelCount to set
	 */
	public void setHotelCount(int hotelCount) {
		HotelCount = hotelCount;
	}
	/**
	 * @param hotelList the hotelList to set
	 */
	public void setHotelList(Map<String, Hotel> hotelList) {
		HotelList = hotelList;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}
	/**
	 * @return the availabilityStatus
	 */
	public String getAvailabilityStatus() {
		return AvailabilityStatus;
	}
	/**
	 * @param availabilityStatus the availabilityStatus to set
	 */
	public void setAvailabilityStatus(String availabilityStatus) {
		AvailabilityStatus = availabilityStatus;
	}
	
	

}
