package com.rezgateway.automation.pojo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CancellationRequest {

	private String CancellationRequestTimestamp 					= null;
	private String UserName 										= "TestDynamicRateTO";
	private String Password 										= "ty5@test12345";
	private String SupplierReferenceNo 								= null;
	private String CancellationReason 								= "Automation_ Default_Reson";
	private String CancellationNotes 								= "test";

	/**
	 * @return the cancellationRequestTimestamp
	 */

	public CancellationRequest getDefaultValues() {

		CancellationRequest resObJ = new CancellationRequest();
		resObJ.setUserName("TesthotelName");
		resObJ.setPassword("Akila");
		
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		
		resObJ.setCancellationRequestTimestamp(date.format(formatter));
		resObJ.setSupplierReferenceNo("362528317X");
		resObJ.setCancellationNotes("Automation");
		resObJ.setCancellationReason("Automation_Test");
		return resObJ;
	}

	public String getCancellationRequestTimestamp() {
		return CancellationRequestTimestamp;
	}

	/**
	 * @param cancellationRequestTimestamp
	 *            the cancellationRequestTimestamp to set
	 */
	public void setCancellationRequestTimestamp(String cancellationRequestTimestamp) {
		CancellationRequestTimestamp = cancellationRequestTimestamp;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return UserName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		UserName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return Password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		Password = password;
	}

	/**
	 * @return the supplierReferenceNo
	 */
	public String getSupplierReferenceNo() {
		return SupplierReferenceNo;
	}

	/**
	 * @param supplierReferenceNo
	 *            the supplierReferenceNo to set
	 */
	public void setSupplierReferenceNo(String supplierReferenceNo) {
		SupplierReferenceNo = supplierReferenceNo;
	}

	/**
	 * @return the cancellationReason
	 */
	public String getCancellationReason() {
		return CancellationReason;
	}

	/**
	 * @param cancellationReason
	 *            the cancellationReason to set
	 */
	public void setCancellationReason(String cancellationReason) {
		CancellationReason = cancellationReason;
	}

	/**
	 * @return the cancellationNotes
	 */
	public String getCancellationNotes() {
		return CancellationNotes;
	}

	/**
	 * @param cancellationNotes
	 *            the cancellationNotes to set
	 */
	public void setCancellationNotes(String cancellationNotes) {
		CancellationNotes = cancellationNotes;
	}

}
