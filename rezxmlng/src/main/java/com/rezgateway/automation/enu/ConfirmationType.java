package com.rezgateway.automation.enu;

public enum ConfirmationType {
	CON, REQ, NONE;

	public static ConfirmationType getConfirmationType(String type) {

		if (type.trim().equalsIgnoreCase("CON")) {
			return ConfirmationType.CON;
		} else if (type.trim().equalsIgnoreCase("REQ")) {
			return ConfirmationType.REQ;
		} else {
			return ConfirmationType.NONE;
		}
	}

}
