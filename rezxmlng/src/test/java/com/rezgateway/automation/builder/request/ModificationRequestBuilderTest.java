package com.rezgateway.automation.builder.request;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.ModificationRequest;
import com.rezgateway.automation.pojo.Room;

public class ModificationRequestBuilderTest {
	
	ModificationRequest resObJ = new ModificationRequest();
	ModificationRequestBuilder builderObj = new ModificationRequestBuilder();

	Room r1 = new Room();
	Room r2 = new Room();

	@Before
	public void Setup() {
		
	/*	if (type.trim().equals("1")) {
			return ModificationType.TOUROPERAORNUM;
		} else if (type.trim().equals("2")) {
			return ModificationType.ADDROOM;
		} else if (type.trim().equals("3")) {
			return ModificationType.CHANGEROOM;
		} else if (type.trim().equals("4")) {
			return ModificationType.STAYPERIOD;
		} else if (type.trim().equals("5")) {
			return ModificationType.AMENDNOTE;
		} else if (type.trim().equals("6")) {
			return ModificationType.GUESTDETAILS;
		} else {
			return ModificationType.NONE;
		}*/

		resObJ.setUserName("TesthotelName");
		resObJ.setPassword("Akila");
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		resObJ.setModificationDetailsTimeStamp(date.format(formatter));
		resObJ.setReferenceNo("362528317X");
		resObJ.setConfType("CON");
		resObJ.setModifyType("1");
		resObJ.setTourOperatorOrderNumber("20113217T03:32:20");
		resObJ.setCheckin("24-MAR-2018");
		resObJ.setCheckout("28-MAR-2018");
		resObJ.setNoofNights("4");
		resObJ.setHotelCode("H00989");
		resObJ.setCurrency("USD");
		resObJ.setTotal("10000");
		resObJ.setTotalaTax("899");

		r1.setRoomNo("1");
		r1.setRoomCode("STD");
		r1.setRoomTypeID("1002");
		r1.setRatePlanCode("ALL");
		r1.setBedTypeID("DUB");
		r1.setAdultsCount("2");
		r1.setChildCount("1");
		String[] namearray1 = new String[2];
		namearray1[0] = "Mr_Akila Samaranayake";
		namearray1[1] = "MS_Rasoja Samaranayake";
		r1.setAdultsOccpancy(namearray1);
		String[] namearray2 = new String[1];
		namearray2[0] = "Child_Sumiuru Gihan";
		r1.setChildOccupancy(namearray2);

		String[] childAges1 = new String[1];
		childAges1[0] = "9";
		r1.setChildAges(childAges1);

		r2.setRoomNo("2");
		r2.setRoomCode("DEL");
		r2.setRoomTypeID("1003");
		r2.setBedTypeID("TPL");
		r2.setAdultsCount("1");
		r2.setChildCount("1");
		String[] namearray3 = new String[1];
		namearray3[0] = "Miss_Rasoja Samaranayake";
		r2.setAdultsOccpancy(namearray3);
		
		String[] namearray4 = new String[1];
		namearray4[0] = "Child_RR Samaranayake";
		//namearray4[1] = "Child_DD Samaranayake";
		r2.setChildOccupancy(namearray4);
		String[] childAges2 = new String[1];
		childAges2[0] = "3";
		//childAges2[1] = "4";
		r2.setChildAges(childAges2);
		
		ArrayList<Room> roomList = new ArrayList<Room>();
		roomList.add(r1);
		roomList.add(r2);
		resObJ.setRoomList(roomList);
		resObJ.setUserComment("new Comment Teseting ");
	}

	@Test
	public void TestSuite() throws IOException {

		String URL = "Resources/Sample_"+resObJ.getModifyType().toString()+"_ModificationRequest.xml";
		String out = null;
		try {
			out = builderObj.buildRequest(URL, resObJ);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(out);
	}

	@After
	public void tearDown() {

		try {
			System.out.println("Test is Finshed ");
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
